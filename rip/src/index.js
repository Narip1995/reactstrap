import React from 'react';
import ReactDOM from 'react-dom';

import App from './testComponents/App';

import 'bootstrap/dist/css/bootstrap.css';


ReactDOM.render(<App/>, document.querySelector("#root"));