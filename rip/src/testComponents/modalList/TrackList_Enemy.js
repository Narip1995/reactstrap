import React from 'react';
import {Table} from 'reactstrap';

export default class Enemy extends React.Component{
  render(){
    return(
      <Table size="sm">
        <thead>
          <tr>
            <th>Enemy</th>
            <th>Category </th>
            <th >Position <a className="text-danger">UTM</a></th>
            <th>Altitude</th>
            <th>Speed <a className="text-danger">m/n</a></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Call</td>
            <td>Call</td>
            <td>Call</td>
            <td>Call</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>Call</td>
            <td>Call</td>
            <td>Call</td>
            <td>Call</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>Call</td>
            <td>Call</td>
            <td>Call</td>
            <td>Call</td>
          </tr>
        </tbody>
      </Table>
    );
  }
}