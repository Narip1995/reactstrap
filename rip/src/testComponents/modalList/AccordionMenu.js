import React from 'react';
import '../../css/AccordionMenu.css';
import TrackList_Total from './TrackList_Total';
import TrackList_Enemy from './TrackList_Enemy';

export default class menu extends React.Component {

    render() {
        return (
            <div class="container" >
                <div class="row btn-block" >
                    <div class="col-sm-auto col-md-auto">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <button type="button" class="btn3d btn3d btn3d-default btn-lg btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    <span class="glyphicon glyphicon-download-alt"></span>Total</button>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">                  
                                                   <TrackList_Total/>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                      
                                        <button type="button" class="btn3d btn3d btn3d-primary btn-lg btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <span class="glyphicon glyphicon-th">
                                        </span>Enemy</button>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                                  <TrackList_Enemy/>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title ">
                                    <button type="button" class="btn3d btn3d btn3d-success btn-lg btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <span class="glyphicon glyphicon-user">
                                        </span>Friend</button>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                                   <a>Test3</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <button type="button" class="btn3d btn3d btn3d-info btn-lg btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <span class="glyphicon glyphicon-file">
                                        </span>Unkown</button>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                                    <a>Test4</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <button type="button" class="btn3d btn3d btn3d-warning btn-lg btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <span class="glyphicon glyphicon-file">
                                        </span>Arthur</button>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    <a>Test5</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <button type="button" class="btn3d btn3d btn3d-danger btn-lg btn-block" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <span class="glyphicon glyphicon-file">
                                        </span>G180</button>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    <a>Test4</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        );
    }
}