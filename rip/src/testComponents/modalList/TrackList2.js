import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import TrackList_Total from './TrackList_Total';
import TrackList_Enemy from './TrackList_Enemy';

export default class TrackList extends React.Component {


    state = {
        activeTab: '1'
    };


    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div >
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="badge badge-pill badge-primary" id="pills-Total-tab" data-toggle="pill" href="#pills-Total" role="tab"
                            aria-con-controls="pills-Total" aria-selected="true">Total</a>
                    </li>
                    <li class="nav-item">
                        <a class="badge badge-pill badge-secondary" id="pills-Enemy-tab" data-toggle="pill" href="#pills-Enemy" role="tab" 
                        aria-controls="pills-Enemy" aria-selected="false">Enemy</a>
                    </li>
                    <li class="nav-item">
                        <a class="badge badge-pill badge-success" id="pills-Friend-tab" data-toggle="pill" href="#pills-Ene-Friend" role="tab" 
                        aria-controls="pills-Ene-Friend" aria-selected="false">Friend</a>
                    </li>
                    <li class="nav-item">
                        <a class="badge badge-pill badge-danger" id="pills-Unknown-tab" data-toggle="pill" href="#pills-Unknown" role="tab" 
                        aria-controls="pills-Unknown" aria-selected="false">Unknown</a>
                    </li>
                    <li class="nav-item">
                        <a class="badge badge-pill badge-warning" id="pills-G180-tab" data-toggle="pill" href="#pills-G180" role="tab" 
                        aria-controls="pills-G180" aria-selected="false">G180</a>
                    </li>
                </ul>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-Total" role="tabpanel" aria-labelledby="pills-Total-tab"><TrackList_Total/></div>
                    <div class="tab-pane fade" id="pills-Enemy" role="tabpanel" aria-labelledby="pills-Enemy-tab"><TrackList_Enemy/></div>
                    <div class="tab-pane fade" id="pills-Friend-" role="tabpanel" aria-labelledby="pills-Friend-tab">...</div>
                    <div class="tab-pane fade" id="pills-Unknown" role="tabpanel" aria-labelledby="pills-Unknown-tab">...</div>
                    <div class="tab-pane fade" id="pills-G180" role="tabpanel" aria-labelledby="pills-G180-tab">...</div>
                    

                </div>
            </div>
        );
    }
}