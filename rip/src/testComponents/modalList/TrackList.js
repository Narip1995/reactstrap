import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import TrackList_Total from './TrackList_Total';
import TrackList_Enemy from './TrackList_Enemy';

export default class TrackList extends React.Component {
 
 
   state = {
      activeTab: '1'
    };


  toggle =(tab)=> {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}>
              Total
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}>
              Enemy
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}>
              Friend
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '4' })}
              onClick={() => { this.toggle('4'); }}>
              Unknown
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '5' })}
              onClick={() => { this.toggle('5'); }}>
              G180
            </NavLink>
          </NavItem>
        </Nav>  

        <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <TrackList_Total/>
            </TabPane>
        </TabContent>

        <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="2">
              <TrackList_Enemy/>
            </TabPane>
        </TabContent>

        <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="3">

            </TabPane>
        </TabContent>
      </div>
    );
  }
}