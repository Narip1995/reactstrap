import React from 'react';


export default class total extends React.Component {
    render() {
        return (
            <div class="container">
            
                <div class="btn-group">
                    <a class="btn-sm btn btn-secondary">Track No
                    
                        {/* <button type="button" class="btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="Toggle_TrackNo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button> */}
                        <input class="search_icon" type="text" name="" placeholder="Search..."></input>
                        <a href="#" class="search_icon"><i class="fas fa-search"></i></a>    
                    </a>
                </div>

                <div class="btn-group">
                    <a class="btn-sm btn btn-secondary">Category</a>
                    <div class="btn-group dropright" role="group">
                        <button type="button" class="btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="Toggle_Category" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    </div>
                </div>

                <div class="btn-group">
                    <a class="btn-sm btn btn-secondary">Position</a>
                    <div class="btn-group dropright" role="group">
                        <button type="button" class="btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="Toggle_Position" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    </div>
                </div>

                <div class="btn-group">
                    <a class="btn-sm btn btn-secondary">Altitude</a>
                    <div class="btn-group dropright" role="group">
                        <button type="button" class="btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="Toggle_Altitude" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    </div>
                </div>

                <div class="btn-group">
                    <a class="btn-sm btn btn-secondary">Speed</a>
                    <div class="btn-group dropright" role="group">
                        <button type="button" class="btn-sm btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="Toggle_Speed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    </div>
                </div>


            </div>
        );
    }
}