import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  //code 1 fragment : copy this code, BEGIN 1
  Button,
  Alert
  //END 1
} from 'react-native';

//code 2 fragment : copy this code, BEGIN 2
const data = require('./data.json');
//END 2

export default class AwesomeProject extends Component {
  //code 3 fragment : copy this code, BEGIN 3
  json_function = () => {

    var json_fragment = data.country.town;
     Alert.alert(
      'Json Text',
      json_fragment
    );
  }
  render() {
    return (
      <View>
        <Button       
          title="Go"
          onPress={this.json_function}
        />
      </View>   
    );
  }
  //END 3
}

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);