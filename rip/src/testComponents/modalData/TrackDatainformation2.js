import React from 'react';
import { Input, Form, FormGroup, Label, FormText, pattern, Col } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import $ from 'jquery/dist/jquery.js';

export default class DataInformation extends React.Component {
  componentDidMount() {
    $(document).ready(function () {
      $('#IFF1,#IFF2,#IFF3,#LTF,#Source,#SpecType,#c').keydown(function (event) {
         // Allow:  delete, tab, enter and . - , f5
         if ($.inArray(event.keyCode, [8, 9, 27, 13, 46, 109, 110, 190, 189, 116]) !== -1 ||
         // Allow: Ctrl+A and Ctrl+C
         (((event.keyCode == 65 )||(event.keyCode == 67)) && event.ctrlKey === true) ||
         // Allow: home, end, left, right
         (event.keyCode >= 35 && event.keyCode <= 39)){
          return;
        }
        else {
          if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
          }
        }
      });
    });
  }

  render() {
    let max = 10, min = 4
    return (

      <AvForm >
        <div className="container">
          <FormGroup row >
            <Label className="col-4 align-items-start"><h6>IFF1</h6></Label>
            <Col sm-md={6}>
              <AvField id="IFF1" name="IFF1" className=" form-control-sm col-md-11 col-sm-11 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>IFF2</h6></Label>
            <Col>
              <AvField id="IFF2" name="IFF2" className="form-control form-control-sm col-md-11 col-sm-11 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>IFF3</h6></Label>
            <Col >
              <AvField id="IFF3" name="IFF3" className="form-control form-control-sm col-md-11 col-sm-11 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col >
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>LTF</h6></Label>
            <Col >
              <AvField id="LTF" name="LTF" className="form-control form-control-sm col-md-11 col-sm-11 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col >
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Source</h6></Label>
            <Col >
              <AvField id="Source" name="Source" className="form-control form-control-sm col-md-11 col-sm-11 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col >
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Spec<span>&nbsp;</span>Type</h6></Label>
            <Col >
              <AvField id="SpecType" name="SpecType" className="form-control form-control-sm col-md-11 col-sm-11 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col >
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>c/s</h6></Label>
            <Col >
              <AvField id="c" name="c" className="form-control form-control-sm col-md-11 col-sm-11 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col >
          </FormGroup>
        </div>
      </AvForm>

    );
  }
}
