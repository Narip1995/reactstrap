import React from 'react';
import JSON from './components/CourseShow';

import './App.css';

function App() {
  return (
    <div className="App">
      <JSON/>
    </div>
  );
}

export default App;
